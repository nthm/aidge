#!/bin/bash

find . -maxdepth 1 -type f -name "clean.sh" ! -name "clean.sh" -exec {} \;

# Function to display help
display_help() {
    echo "Usage: $0 [--python|-p] [--graph|-g] [--build|-b] [--all] [--help|-h]"
    echo "Options:"
    echo "  --python, -p: Uninstall all Aidge related packages from the current Python environment"
    echo "  --graph, -g:  Remove recursively every Mermaid graph from the current directory"
    echo "  --build, -b:  Remove every 'build*', '*.egg-info' and the Aidge install directories"
    echo "  --all:        Run all tasks"
    echo "  --help, -h:   Display this help message"
    exit 0
}

# --python function
clear_python_env() {
    echo "Cleaning current Python environment..."
    pip list | grep 'aidge' | cut -d ' ' -f 1 | xargs -r pip uninstall -y
}

# --graph function
clear_mermaid_graphs() {
    # Use find to locate files with the .mmd extension and print the list
    echo "Cleaning Mermaid graphs..."
    files_to_remove=$(find "$(pwd)" -type f -name "*.mmd")
    echo "$files_to_remove"
    find "$(pwd)" -type f -name "*.mmd" -exec rm -f {} +
}

# clear build function
clear_build() {
    # remove install directory
    if [[ -z "${AIDGE_INSTALL}" ]]; then
        export AIDGE_INSTALL="$(pwd)/aidge/install"
    fi
    echo "clean ${AIDGE_INSTALL}"
    rm -rf ${AIDGE_INSTALL}

    # remove build directories
    BUILDPATHS=()

    ## Use the find command to search for directories named "build"
    ## in the current directory and its subdirectories
    while IFS= read -r -d '' dir; do
    # Add each directory to the BUILDPATHS list
    BUILDPATHS+=("$dir")
    done < <(find ./aidge -type d -name "build*" -print0)

    ## Print and remove the list of build paths
    for path in "${BUILDPATHS[@]}"; do
    echo "clean $path"
    rm -rf ${path}
    done

    # remove egg-info
    EGGINFOPATHS=()
    while IFS= read -r -d '' dir; do
        EGGINFOPATHS+=("$dir")
    done < <(find . -type d -name "*.egg-info" -print0)

    for path in "${EGGINFOPATHS[@]}"; do
        echo "clean $path"
        rm -rf ${path}
    done
}

# main loop
# parse cmd line args
# Parse command line arguments
while [[ $# -gt 0 ]]; do
    case $1 in
        --python|-p)
            RUN_PYTHON=true
            shift
            ;;
        --graph|-g)
            RUN_GRAPH=true
            shift
            ;;
        --build|-b)
            RUN_BUILD=true
            shift
            ;;
        --all)
            RUN_PYTHON=true
            RUN_GRAPH=true
            RUN_BUILD=true
            shift
            ;;
        --help|-h)
            display_help
            ;;
        *)
            echo "Unknown option: $1"
            echo "Use --help or -h for usage information"
            exit 1
            ;;
    esac
done


# Execute tasks based on options
if [ "$RUN_PYTHON" = true ]; then
    clear_python_env
fi

if [ "$RUN_GRAPH" = true ]; then
    clear_mermaid_graphs
fi

if [ "$RUN_BUILD" = true ]; then
    clear_build
fi

# If no options were provided, display usage
if [ -z "$RUN_PYTHON" ] && [ -z "$RUN_GRAPH" ] && [ -z "$RUN_BUILD" ]; then
    display_help
fi