import math

class ArmConv2DDims:
        def __init__(self, node):
            self.operator = node.get_operator()
            # Add in and out channels as attributes before the weight interleaving
            # 1. For the generation the scheduling 
            # 2. To correct the in_chan and out_chan exported by node_export
            outchannels = self.operator.get_input(1).dims()[0]
            inchannels = self.operator.get_input(1).dims()[1]
            self.operator.attr.add_attr("out_channels", outchannels)
            self.operator.attr.add_attr("in_channels", inchannels)


        def _check_input_dim(self, input):
            # Also suppose x[0] is a 4D tensor 
            if len(input) != 4:
                raise ValueError(f"{self.operator().type()} expected 4D input (got {input.dim()}D input)")

        def compute(self, x):
            # Suppose x is [inputs, weights, biases]
            inputs = x[0]

            # if inputs is None, the scheduler has not provided the dimensions yet
            # Wait for next call
            if not inputs:
                return [[]]
            
            self._check_input_dim(inputs)
            y = [0] * 4
            
            attributes_conv = self.operator.attr.get_attr("Conv2D_0")

            kernel = attributes_conv.get_attr("kernel_dims")
            stride = attributes_conv.get_attr("stride_dims")
            dilation = attributes_conv.get_attr("dilation_dims")
            out_channel = self.operator.attr.get_attr("out_channels")

            padding = [0, 0]
            if self.operator.attr.has_attr("Pad2D_0"):
                if self.operator.attr.get_attr("Pad2D_0").has_attr("BeginEndBorders"):
                    padding = self.operator.attr.get_attr("Pad2D_0").get_attr("BeginEndBorders")
                
            
            # Batch N
            y[0] = inputs[0]
            # Out channels C
            y[1] = out_channel
            # Out Height H 
            y[2] = math.floor(1 + ((inputs[2] + 2 * padding[0] - dilation[0] * (kernel[0] - 1) - 1) / stride[0]))
            # Out Width W 
            y[3] = math.floor(1 + ((inputs[3] + 2 * padding[1] - dilation[1] * (kernel[1] - 1) - 1) / stride[1]))
            return [y]
        

class ArmFCDims:
        def __init__(self, node):
            self.node = node
            self.operator = node.get_operator()

            # Add in and out channels as attributes before the weight interleaving
            # 1. For the generation the scheduling 
            # 2. To correct the in_chan and out_chan exported by node_export
            outchannels = self.operator.get_input(1).dims()[0]
            inchannels = self.operator.get_input(1).dims()[1]
            self.operator.attr.add_attr("out_channels", outchannels)
            self.operator.attr.add_attr("in_channels", inchannels)

        def compute(self, x):
            # Suppose x is [inputs, weights, biases]
            inputs = x[0]

            # if inputs is None, the scheduler has not provided the dimensions yet
            # Wait for next call
            if not inputs:
                return [[]]
            
            y = [0] * 2
            y[0] = inputs[0]
            y[1] = self.operator.attr.get_attr("out_channels")
            return [y]