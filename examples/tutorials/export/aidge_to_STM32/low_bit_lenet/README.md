# Low bit ARM Cortex-M export toolchain 

Currently the low bit Arm export toolchain in Aidge is the following :
![Low bit export toolchain](static/image.png)

The pdf presentation in the static folder explains each step of this toolchain.

Further improvements on this toolchain are discussed in the pdf presentation.

# File structure 

- ``export_lenet_int4.py`` demonstrate the low bit export toolchain in Aidge
- ``forward_dims.py`` implement the forward dims functions required for the generic Operators

# Run 

Build the following aidge modules in your environnement : 
```
./setup.sh -m core -m backend_cpu -m backend_cuda -m quantization -m onnx -m export_arm_cortexm --python --debug
```

Generate the Lib folder (for STM32F7): 
```
python export_lenet_int4.py
```

To build the export, adapt the model forward function : 
- Add input to the model forward function as uint8 then cast it into udata<8> and cast the output into int8

```
void model_forward (uint8_t* input, int8_t** output_ptr)
{
    udata<8>* input = reinterpret_cast<udata<8>*>(input_0);

    ...

    *outputptr = reinterpret_cast<int8_t*>(last_output);
}
```


- Add the following application code in the main.cpp
```
printf("\r\n");
  printf("*****************************************************\r\n");
  printf("****************** DEMO EXPORT CPP ******************\r\n");
  printf("*****************************************************\r\n");
  printf("\r\n");


  const unsigned int nb_classes = 10;

  int8_t results[nb_classes];
  int8_t* output_array = results;

  model_forward(inputs, output_array);

  for (unsigned int i = 0; i < nb_classes; ++i)
  {
    printf("\r\n %d, %d", i, results[i]);
  }


  printf("\r\n");
  printf("*****************************************************\r\n");
  printf("********************** END DEMO *********************\r\n");
  printf("*****************************************************\r\n");
  printf("\r\n");
```

We provide tools to build the binary using docker.
If you don't already have the docker image, create the docker image : 
```
make build_image_docker
```

Then build the binary using a docker :
```
make build_export_docker
```

Once you have the binary, flash with STMCubeProgrammer on your board.

# Current Issues

- Debugging phase of LeNet inference to reproduce results 
- Adaptation of Mem manager for weightInterleaving + Templating sub-8bit kernels with memory info
- Missmatch between attributes without compression (needed by the sub 8 bits kernel loops) and the memory to allocate for compressed activations 
- Compilation error on headers from H7 configurations
- Compilation error for custom datatype data<n> when used in model_forward signature (temp workaround int8 in signature with reinterpret_cast)
- In applyWeightInterleaving recipe, handle FC weights (2D) appart from 4D weights (conv) because .setDataformat soes not support 2D tensors
- The ``compact_data_during_loop`` fonctions compact the activations in the kernels. Did not test the function in other settings than 4bits. Suspicions of error for 3 bits integers (the shift is 3 while it should be 4).
- Add Apply Weight Interleaving to recipe « adaptToBackend »
