{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial: Using TensorRT 8-Bit Quantization with Aidge \n",
    "\n",
    "In this tutorial, we'll walk through the process of performing 8-bit quantization on a simple model using TensorRT and Aidge. <br>\n",
    "The steps include:\n",
    "- exporting the model\n",
    "- modifying the test script for quantization\n",
    "- preparing calibration data\n",
    "- running the quantization and profile the quantized model\n",
    "\n",
    "![tutorial graph](draw.png)\n",
    "\n",
    "Furthermore, as shown in this image but not demonstrated in this tutorial, Aidge allows the user to:\n",
    "- Add custom operators via the plugin interface\n",
    "- Facilitate the transformation of user data into calibration data\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 0. Requirements for this tutorial\n",
    "\n",
    "To complete this tutorial, we hightly recommend following these requirements:\n",
    "- To have completed the [Aidge 101 tutorial](https://gitlab.eclipse.org/eclipse/aidge/aidge/-/tree/master/examples/tutorials/Aidge_tutorial?ref_type=heads)\n",
    "- To have installed the `aidge_export_tensorrt` module\n",
    "\n",
    "In order to compile the export on your machine, please be sure to have one of these two conditions:\n",
    "- To have installed [Docker](https://docs.docker.com/get-docker/) (the export compilation chain is able to use docker)\n",
    "- To have installed the correct packages to support TensorRT 8.6"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Exporting the model\n",
    "\n",
    "In this tutorial, we will export [MobileNetV2](https://github.com/onnx/models/tree/main/validated/vision/classification/mobilenet/model), a lightweight convolutional neural network. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!wget -c https://github.com/onnx/models/raw/main/validated/vision/classification/mobilenet/model/mobilenetv2-7.onnx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For visualizing the model structure, we recommend using Netron. <br>\n",
    "If you haven't installed Netron yet, you can do so by executing the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install netron"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once installed, you can launch Netron to visualize the model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import netron\n",
    "netron.start('mobilenetv2-7.onnx', 8080)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then let's export the model using the `aidge_export_tensorrt` module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First, be sure that any previous exports are removed\n",
    "!rm -rf export_trt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aidge_export_tensorrt\n",
    "\n",
    "# Generate export for your model \n",
    "# This function takes as argument the name of the export folder \n",
    "# and the onnx file or the graphview of your model\n",
    "aidge_export_tensorrt.export(\"export_trt\", \"mobilenetv2-7.onnx\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The export povides a Makefile with several options to use the export on your machine. <br>\n",
    "You can generate a C++ export or a Python export.\n",
    "\n",
    "You also have the possibility to compile the export or/and the Python library by using Docker if your host machine doesn't have the correct packages. <br>\n",
    "In this tutorial, we generate the Python library of the export and use it a Python script.\n",
    "\n",
    "All of these options are resumed in the helper of the Makefile (run `make help` in the export folder for more details)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compile the export Python library by using docker \n",
    "# and the Makefile provided in the export\n",
    "!cd export_trt/ && make build_lib_python_docker"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Modifying the test script for quantization\n",
    "\n",
    "Next, you have to modify `test.py` by adding `nb_bits=8` in the graph constructor and call `model.calibrate()`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`calibrate()` can accept three arguments: \n",
    "- **calibration_folder_path**: to specify the path to your calibration folder \n",
    "- **cache_file_path**: to use your pre-built calibration cache\n",
    "- **batch_size**: to specify the batch size for calibration data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile export_trt/test.py\n",
    "\"\"\"Example test file for the TensorRT Python API.\"\"\"\n",
    "\n",
    "import build.lib.aidge_trt as aidge_trt\n",
    "import numpy as np\n",
    "\n",
    "if __name__ == '__main__':\n",
    "    # Load the model\n",
    "    model = aidge_trt.Graph(\"model.onnx\", nb_bits=8)\n",
    "\n",
    "    # Calibrate the model\n",
    "    model.calibrate()\n",
    "\n",
    "    # Initialize the model\n",
    "    model.initialize()\n",
    "\n",
    "    # Profile the model with 10 iterations\n",
    "    model.profile(10)\n",
    "\n",
    "    # Example of running inference\n",
    "    # img: numpy.array = np.load(\"PATH TO NPY file\")\n",
    "    # output: numpy.array = model.run_sync([img])\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Preparing the calibration dataset\n",
    "\n",
    "To ensure accurate calibration, it's essential to select representative samples. In this example, we will use a 224x224 RGB image from the ImageNet dataset.\n",
    "\n",
    "However, for practical applications, TensorRT suggests that \"The amount of input data required is application-dependent, but experiments indicate that approximately 500 images are adequate for calibrating ImageNet classification networks\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create calibration folder\n",
    "!cd export_trt/ && mkdir calibration_folder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.image as mpimg\n",
    "\n",
    "demo_img_path = './data/0.jpg'\n",
    "\n",
    "img = mpimg.imread(demo_img_path)\n",
    "imgplot = plt.imshow(img)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This image has been preprocessed and stored in `/data/` as `0.batch` file. Information about the image's shape is stored in the `.info` file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import shutil\n",
    "\n",
    "shutil.copy(\"data/.info\", \"export_trt/calibration_folder/.info\")\n",
    "shutil.copy(\"data/0.batch\", \"export_trt/calibration_folder/0.batch\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Generating the quantized model \n",
    "\n",
    "Finally, run the test script to quantize the model with the export python library and profile it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cd export_trt/ && make test_lib_python_docker"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Following these steps have enabled you to conduct 8-bit quantization on your model. <br> \n",
    "Upon completing the calibration, the calibration data can be reused if a `calibration_cache` exists, saving computational resources. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!tail -n +0 export_trt/calibration_cache"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After quantization, feel free to save the generated TensorRT engine using `model.save(\"name_of_your_model\")`. The method will save the engine into a `.trt` file. \n",
    "\n",
    "To load the engine for further applications, use `model.load(\"name_of_your_model.trt\")` after instancing a model."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env_aidge",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
