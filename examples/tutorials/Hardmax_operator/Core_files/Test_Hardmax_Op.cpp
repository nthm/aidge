#include <catch2/catch_test_macros.hpp>

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Hardmax.hpp"
#include "aidge/operator/OperatorTensor.hpp"

namespace Aidge {
    TEST_CASE("[core/operator] Hardmax_Op(forwardDims)", "[Hardmax][forwardDims]") {

        SECTION("Hardmax Op") {
            // Create Hardmax Operator
            auto myHardmax = Hardmax();
            auto op = std::static_pointer_cast<OperatorTensor>(myHardmax->getOperator());

            // Associate input
            auto myTensor = std::make_shared<Tensor>();
            op->associateInput(0, myTensor);

            SECTION("Hardmax Scalar") {
                myTensor->resize({});
                REQUIRE_NOTHROW(op->forwardDims());
                REQUIRE((op->getOutput(0)->dims() == std::vector<std::size_t>()));
            }

            SECTION("Hardmax 3D") {
                myTensor->resize({2, 3, 4});
                REQUIRE_NOTHROW(op->forwardDims());
                REQUIRE((op->getOutput(0)->dims() == std::vector<std::size_t>({2, 3, 4})));
            }
        }
    }
}
