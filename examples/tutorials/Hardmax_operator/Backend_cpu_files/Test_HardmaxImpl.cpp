/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <catch2/catch_test_macros.hpp>
#include <memory>
#include <numeric>   // std::accumulate
#include <random>    // std::random_device, std::mt19937, std::uniform_real_distribution

#include "aidge/data/Tensor.hpp"
#include "aidge/operator/Hardmax.hpp"
#include "aidge/operator/Conv.hpp"

#include "aidge/backend/cpu.hpp"
#include "aidge/utils/TensorUtils.hpp"

using namespace Aidge;

TEST_CASE("[cpu/operator] Hardmax(forward)", "[Hardmax][CPU]") {

    SECTION("3D Tensor") {
        std::shared_ptr<Tensor> myInput = std::make_shared<Tensor>(Array3D<float,2,3,4> {
                {
                    {
                        { 1.0, 2.0, 3.0, 4.0},
                        { 8.0, 0.0, 17.0, 1.0},
                        { 5.0, 10.0, 6.0, 0.0}
                    },
                    {
                        { 7.0, 1.0, 9.0, 4.0},
                        { 0.0, 8.0, 4.0, 2.0},
                        { 9.0, 2.0, 0.0, 5.0}
                    }
                }
            });
        SECTION("Axis 2") {
            Tensor myOutput = Tensor(Array3D<float,2,3,4> {
                {
                    {
                        { 0.0, 0.0, 0.0, 1.0},
                        { 0.0, 0.0, 1.0, 0.0},
                        { 0.0, 1.0, 0.0, 0.0}
                    },
                    {
                        { 0.0, 0.0, 1.0, 0.0},
                        { 0.0, 1.0, 0.0, 0.0},
                        { 1.0, 0.0, 0.0, 0.0}
                    }
                }
            });

            std::shared_ptr<Node> myHardmax = Hardmax(2);
            auto op = std::static_pointer_cast<OperatorTensor>(myHardmax->getOperator());
            op->associateInput(0, myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            myHardmax->forward();

            REQUIRE(*(op->getOutput(0)) == myOutput);
        }

        SECTION("Axis 1") {
            Tensor myOutput = Tensor(Array3D<float,2,3,4> {
                {
                    {
                        { 0.0, 0.0, 0.0, 1.0},
                        { 1.0, 0.0, 1.0, 0.0},
                        { 0.0, 1.0, 0.0, 0.0}
                    },
                    {
                        { 0.0, 0.0, 1.0, 0.0},
                        { 0.0, 1.0, 0.0, 0.0},
                        { 1.0, 0.0, 0.0, 1.0}
                    }
                }
            });

            std::shared_ptr<Node> myHardmax = Hardmax(1);
            auto op = std::static_pointer_cast<OperatorTensor>(myHardmax->getOperator());
            op->associateInput(0, myInput);
            op->setDataType(DataType::Float32);
            op->setBackend("cpu");
            myHardmax->forward();

            REQUIRE(*(op->getOutput(0)) == myOutput);
        }
    }
}
