/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#ifndef AIDGE_CPU_OPERATOR_HARDMAXIMPL_FORWARD_KERNEL_H_
#define AIDGE_CPU_OPERATOR_HARDMAXIMPL_FORWARD_KERNEL_H_

#include <cmath>
#include <cstddef>
#include <numeric>

#include "aidge/backend/cpu/operator/HardmaxImpl.hpp"
#include "aidge/operator/Hardmax.hpp"
#include "aidge/data/Data.hpp"
#include "aidge/utils/Registrar.hpp"

namespace Aidge {
template <class I, class O>
void HardmaxImpl_cpu_forward_kernel(std::int32_t axis_, const std::vector<DimSize_t>& dims, const void* input_, void* output_)
{
    // We start by casting our arguments
    const I* input = static_cast<const I*>(input_);
    O* output = static_cast<O*>(output_);
    // Cast axis to a size_t
    const std::size_t axis = axis_ >= 0 ? axis_: axis_ + dims.size();

    // We fill all the output tensor with 0, we will set to 1 only the max element later
    std::size_t totalElements = std::accumulate(dims.cbegin(), dims.cend(), std::size_t(1), std::multiplies<std::size_t>());
    std::fill(output, output + totalElements, 0);

    std::size_t postAxisStride = 1;
    for (std::size_t i = axis + 1; i < dims.size(); ++i) {
        postAxisStride *= dims[i];
    }
    std::size_t preAxisStride = 1;
    for (std::size_t i = 0; i < axis; ++i) {
        preAxisStride *= dims[i];
    }
    // For each index on all the axes before and after 'axis', we have a different max element to find
    for(std::size_t i=0; i<preAxisStride; ++i)
    {
        for(std::size_t j=0; j<postAxisStride; ++j)
        {
            // Init the max with first element
            std::size_t maxIdx = 0;
            I maxVal = input[i * postAxisStride * dims[axis] + j];
            // Loop over the elements on 'axis'
            for(std::size_t k=1; k<dims[axis]; ++k)
            {
                I currVal = input[i * postAxisStride*dims[axis] + k * postAxisStride + j];
                // Update max elements
                if (currVal > maxVal)
                {
                    maxIdx = k;
                    maxVal = currVal;
                }
            }
            output[i * postAxisStride * dims[axis] + maxIdx * postAxisStride + j] = 1;
        }
    }
}

// Then we add the Registrar declaration for different input/output types
REGISTRAR(HardmaxImpl_cpu,
          {DataType::Float32},
          {ProdConso::defaultModel, Aidge::HardmaxImpl_cpu_forward_kernel<float, float>, nullptr});
REGISTRAR(HardmaxImpl_cpu,
          {DataType::Int32},
          {ProdConso::defaultModel, Aidge::HardmaxImpl_cpu_forward_kernel<std::int32_t, std::int32_t>, nullptr});
REGISTRAR(HardmaxImpl_cpu,
          {DataType::Float64},
          {ProdConso::defaultModel, Aidge::HardmaxImpl_cpu_forward_kernel<double, double>, nullptr});

}  // namespace Aidge

#endif /* AIDGE_CPU_OPERATOR_HARDMAXIMPL_FORWARD_KERNEL_H_ */
