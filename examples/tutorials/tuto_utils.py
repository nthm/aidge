import base64
import zlib
import json
from IPython.display import Image, display
import matplotlib.pyplot as plt
import os
import requests

def download_material(path: str, file: str) -> None:
    if not os.path.isfile(file):
        response = requests.get("https://gitlab.eclipse.org/eclipse/aidge/aidge/-/raw/dev/examples/tutorials/"+path+"/"+file+"?ref_type=heads")
        if response.status_code == 200:
            with open(file, 'wb') as f:
                f.write(response.content)
            print("File downloaded successfully.")
        else:
            print("Failed to download file. Status code:", response.status_code)

def visualize_mmd(path_to_mmd):
  with open(path_to_mmd, "r") as file_mmd:
    graph_mmd = file_mmd.read()

  jGraph = {
          "code": graph_mmd,
          "mermaid": {"theme": "default"}
      }
  byteStr = bytes(json.dumps(jGraph), 'ascii')
  compress = zlib.compressobj(9, zlib.DEFLATED, 15, 8,zlib.Z_DEFAULT_STRATEGY)
  deflated = compress.compress(byteStr)
  deflated += compress.flush()
  dEncode = base64.urlsafe_b64encode(deflated)
  display(Image(url='https://mermaid.ink/img/pako:' + dEncode.decode('ascii')))
