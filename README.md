<picture align="center">
<img src="docs/source/_static/Logotype-aidge.png" alt="Aidge logo" width="800"/>
</picture>

[![License-badge](https://img.shields.io/badge/License-EPL%202.0-blue.svg)](LICENSE) [![Documentation Status](https://readthedocs.org/projects/eclipse-aidge/badge/?version=latest)](https://eclipse-aidge.readthedocs.io/en/latest/?badge=latest)

# Eclipse Aidge

**Aidge is a powerful open-source framework for optimizing and deploying deep neural networks on embedded systems.**

It provides a comprehensive toolkit for manipulating computational graphs, with seamless integration for popular frameworks through ONNX and PyTorch interfaces. Whether you need to quantize models, map them to specialized hardware accelerators, or generate optimized code for various targets (CPU, GPU, FPGA, custom accelerators), Aidge offers a modular and flexible solution. The framework excels in DNN design exploration, allowing rapid prototyping and automated performance benchmarking while supporting multiple hardware export targets including OpenMP, OpenCL, CUDA, cuDNN, and TensorRT.

## Useful Links
- [Documentation](https://eclipse.dev/aidge/)
- [Installation Guide](#installation)
    - [pip](#1-using-pip-recommended-only-available-on-linux) (Recommended): Quickest way to get started
    - [Build](#2-build-from-source) from Source: For development or customization
    - [Docker](#3-using-docker): For containerized deployment
- [Contributing Guidelines](https://gitlab.eclipse.org/groups/eclipse/aidge/-/wikis/Contributing)
- [Issue Tracker](https://gitlab.eclipse.org/groups/eclipse/aidge/-/issues)
- [Available Modules](https://gitlab.eclipse.org/eclipse/aidge)
- [The official chat of the project](https://chat.eclipse.org/#/room/#aidge:matrix.eclipse.org).

## Quick Start

**System Requirements**
- ``Python >= 3.8``

Additional requirements may **vary by module**. Please check individual module documentation for details.

## Installation

Aidge is built on a core library that interfaces with multiple modules binded to python libraries.
**Each module must be installed independently**, `aidge_core` included.

```bash
# We recommend creating a Python environment to work with Aidge
python<your_python_version> -m venv env_aidge
source env_aidge/bin/activate
```

### 1. Using pip (Recommended) (only available on Linux)

Install `aidge_core` and selected modules:

```bash
# install full bundle
# - `aidge_core`
# - `aidge_backend_cpu`
# - `aidge_backend_cuda`
# - `aidge_backend_opencv`
# - `aidge_export_cpp`
# - `aidge_export_arm_cortexm`
# - `aidge_learning`
# - `aidge_onnx`
# - `aidge_quantization`
pip install -r default_module_intall.txt

# install selected modules, for example to import an ONNX, quantize the model and export it to a standalone CPP directory
pip install aidge-core aidge-backend-cpu aidge-learning aidge-export-cpp aidge-onnx aidge-quantization
```

Feel free to install other modules from the [list of availalbe submodules](https://gitlab.eclipse.org/eclipse/aidge) if you need them with this bundle. <br>

### 2. Build from Source

**System requirements**
- ``CMake >= 3.18``

This repository has several [submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
For this reason when cloning this repository you need to use the ``--recursive`` option.

```bash
# Clone repository with submodules
git clone --recursive https://gitlab.eclipse.org/eclipse/aidge/aidge.git
```

#### Build on Linux & MacOS

##### For Python development

```bash
# Build every Python modules
./setup.sh --all --release --python

# Or build specific modules
# Notice how it compiles modules named `aidge_<module_name>` by calling `-m <module_name>`
./setup.sh -m core -m backend_cpu -m onnx --release --python
```

##### For C++ development

```bash
# Build C++ library only by removing `--python`
./setup.sh --all --release

# Or build specific modules
./setup.sh -m core -m backend_cpu --release

# Debug build with tests
./setup.sh -m core -m backend_cpu --debug --tests
```

For more build options, run: `./setup.sh -h`

#### Build on Windows

On your session,
1) Install required tools:
    - Visual Studio Community with MSVC: https://visualstudio.microsoft.com/fr/vs/community/
    - Git (Source code control program): https://git-scm.com/download
    - CMake (Solution for managing the software build process): https://cmake.org/download/
    - Python (includes pip): https://www.python.org/download/
    - Visual Studio Code: https://code.visualstudio.com/download with the following extensions
        - Python
        - CMake
2) Set up Python environment:

```powershell
# Create virtual environment
python -m venv env_aidge

# Activate environment
.\env_aidge\Scripts\activate
```

3) Install Aidge:

```powershell
# Clone repository
git clone --recursive https://gitlab.eclipse.org/eclipse/aidge/aidge.git

# Navigate to repository
cd aidge
```

##### For Python development:

Add the `-Python` option to your command.

```powershell
# Build every Python module in Release mode
.\setup.ps1 -All -Release -Python

# Or build specific modules with Python bindings
.\setup.ps1 -Modules core,backend_cpu,onnx -Release -Python
```

#### For C++ development:

```powershell
# Build C++ library only in Release mode
.\setup.ps1 -All -Release

# Debug build with tests
.\setup.ps1 -Modules core,backend_cpu,onnx -Debug -Tests
```

Run `.\setup.ps1 -h` to see all available options and parameters.

### 3. Using Docker

Feel free to use one of the Dockerfiles available in the [`docker`](docker) folder.

```bash
# Build image
docker build --pull --rm -f "docker/dockerfile.Dockerfile" -t aidge:latest .

# Run container
docker run --name aidge-container -it aidge:latest
```

## Verify installation

```bash
python -c "import aidge_core; import aidge_backend_cpu; print(aidge_core.Tensor.get_available_backends())"
# Expected output: {'cpu'}
```

## Bundle components

### Modules source code

- [core](https://gitlab.eclipse.org/eclipse/aidge/aidge_core)
- [backend]
    - [reference CPU](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu)
    - [CUDA](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda)
    - [OpenCV](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_opencv)
- [ONNX interoperability](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx)
- [export]
    - [reference CPP](https://gitlab.eclipse.org/eclipse/aidge/aidge_export_cpp)
    - [ARM-CortexM](https://gitlab.eclipse.org/eclipse/aidge/aidge_export_arm_cortexm)
- [quantization](https://gitlab.eclipse.org/eclipse/aidge/aidge_quantization)

## Modules status

| Module    | Status | Coverage |
| -------- | ------- | ------- |
| [aidge_core](https://gitlab.eclipse.org/eclipse/aidge/aidge_core)  | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/pipeline.svg?ignore_skipped=true) | ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)    |
| [aidge_backend_cpu](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu) | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/pipeline.svg?ignore_skipped=true) | ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)     |
| [aidge_backend_cuda](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda) | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/pipeline.svg?ignore_skipped=true) | ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)     |
| [aidge_export_cpp](https://gitlab.eclipse.org/eclipse/aidge/aidge_export_cpp) |  |      |
| [aidge_onnx](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx)    | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/badges/main/pipeline.svg?ignore_skipped=true) | ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)    |

## Stay tuned !
Here is a link to [the official matrix chat of the project](https://chat.eclipse.org/#/room/#aidge:matrix.eclipse.org).

## Contributing
If you would like to contribute to the Aidge project, we would be happy to have your help!

Everyone is welcomed to contribute to the code via merge requests, file issues on Gitlab, help people asking for help, or any other way!

To get started you can head to the [wiki home page](https://gitlab.eclipse.org/groups/eclipse/aidge/-/wikis/home) and look for contribution section.

We grant commit access (which allows to work on the official project instead of a fork & gived full rights to the issue database(label modification)) to people who have gained our trust and demonstrated a commitment to Aidge.

## License

Aidge is licensed under Eclipse Public License 2.0, as found in the [LICENSE](LICENSE).
