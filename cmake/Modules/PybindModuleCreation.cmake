function(generate_python_binding name library) 
    Include(FetchContent)

    FetchContent_Declare(
    PyBind11
    GIT_REPOSITORY https://github.com/pybind/pybind11.git
    GIT_TAG        v2.10.4 # or a later release
    )

    FetchContent_MakeAvailable(PyBind11)
    message(STATUS "Creating binding for module ${name}")
    file(GLOB_RECURSE pybind_src_files "python_binding/*.cpp")
    pybind11_add_module(${name} MODULE ${pybind_src_files} "NO_EXTRAS")
    target_include_directories(${name} PUBLIC ${pybind11_INCLUDE_DIRS} "python_binding")
    target_link_libraries(${name} PUBLIC ${library})
endfunction()