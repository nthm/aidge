Graph Matching
==============

Aidge introduces a simple and efficient DSL for graph matching, sometimes called "graph regex". It is possible to write complex textual queries to find a quantified or unquantified set of nodes with specific types, attributes and/or relationships between them. This is particularly useful to implement sophisticated pattern-matching heuristics with no effort!

Here is an example of a query that you can do in Aidge:

.. code-block:: python

    matches = aidge_core.SinglePassGraphMatching(model).match("(Pad#?->Conv#|Deconv#->Pad#?)->ReLU#?->Add#1?->ReLU#?->MaxPool#?->ReLU#?;.->Add#1?")

    for match in matches:
        aidge_core.GraphView.replace(match, MyCustomIPOperator())

You can define your own node test function as well:

.. code-block:: python

    gm = aidge_core.SinglePassGraphMatching(model)
    gm.add_node_lambda("test", lambda node: ...)

    matches = gm.match("Conv->test")


.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.SinglePassGraphMatching
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenclass:: Aidge::SinglePassGraphMatching
