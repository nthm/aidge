Backend
=======


Implementation specification
----------------------------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.ImplSpec
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenstruct:: Aidge::ImplSpec



Input/Output specification
--------------------------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.IOSpec
            :members:
            :inherited-members:

    .. tab-item:: C++

        .. doxygenstruct:: Aidge::ImplSpec::IOSpec
