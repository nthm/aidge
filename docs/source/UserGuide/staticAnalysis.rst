Static analysis
===============

Overview
--------

AIDGE propose a set of functions which compute and aggregate metrics, computed without running an inference, which allow the user to evaluate the model.

Get node KPI
------------

Computational footprint
^^^^^^^^^^^^^^^^^^^^^^^

Give the number of Multiply-ACcumulate (MAC) operations per :ref:`operator <source/userguide/modelGraph:Operator>`.

Memory footprint
^^^^^^^^^^^^^^^^

We can dinstiguish two memory footprints:

* **Compute buffers memory:** Memory footprint of inputs/outputs per :ref:`operator <source/userguide/modelGraph:Operator>`
* **Parameters memory:** Memory footprint of parameters per operator

Aggregate KPI from a subgraph
-----------------------------

Agregate the KPI previously described for a subgraph. This allow to put them on a relative scale.

**Example :** 

.. image:: /source/_static/StaticKPI.PNG
  :align: center

